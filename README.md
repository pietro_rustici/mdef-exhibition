# MDEF - Challenge 2 (16/03/2021 - 19/03/2021) - (Team 5) 
### Jana Tothill Calvo - [personal webpage](https://jana_tothillcalvo.gitlab.io/website/) <br> Pietro Rustici - [personal webpage](https://pietro_rustici.gitlab.io/mdef-website/)

##### Rubric
1. Initial idea/Concept of the Project                                          **(done)**                  
2. Propose (What is supposed to do or not to do)                                **(done)**
3. Shown how your team planned and executed the project                         **(done)**
4. System diagram (illustration explaining function, parts, and relations)      **(done)**
5. Design elements (How you designed it)                                        **(done)**
6. How did you fabricate it (fabrication processes and materials)               **(done)**
7. Design & Fabrication files                                                   **(done)**            
8. BOM (Bill of Materials)                                                      **(done)**
9. Photographies of the end artifacts.                                          **(done)**
10. Iteration Process - Described problems and how the team solved them         **(done)**         
11. Listed future development opportunities for this project                    **(done)**
12. Linked to your individual pages                                             **(done)**                                           

##### Table of content
1. Inspiration
    1. Explanation of what we are doing/inspiration
2. Production
    1. Design
        1. Object
        2. Content of classmates on table
    2. Making
3. Final product 
    1. Assembly 
    2. Exposition 

----
## Initial idea
<img src= "images/cards1.jpeg">
<img src= "images/cards2.jpeg">

## Inspiration
We wanted to create a modular structure to be displayed in the centre of our design dialogues. At the begining We wanted to make the object serve as a welcoming object, almost like an index, that would include all the students of MDEF. This evolved to a central  longitudinal piece that would go throughout the exhibition displaying the process of all of our work in this term.
<br><br>
Initial idea
<img src= "images/cards1.jpeg">
<img src= "images/cards2.jpeg">
<img src= "images/inspirationmdef.jpeg">
<img src= "images/inspirationmdef2.jpeg">

## Bill of Materials (BOM)
| Material  | Quantity  |
|---|---|
|  Plywood 250 x 125 x 1.5 cm |  1 |
|  Leg joints |  7 |
|  Leg adaptor | 2  |
| Screws | 14 |
## Production
Regarding the design we wanted to create a modular system so that it could be adaptable to the space depending on the arrangemente of the stands of each student. Therefore we designed standard lenght tables that would be joined with a piece that would allow rotation. One can find the documents in the design folder of this repo.

<img src= "images/piece1.png">
<img src= "images/piece2.png">
<img src= "images/piece3.png">
<br><br>
These 3 images are the rendered final design of one of the sections of the table. This was generated in solidworks and used assembly to make sure the angles were adecuate for a smooth rotation of the table.

<img src= "images/piece4.png">
<br><br><br>
This is the joint that holds the two pieces of wood together, allowing rotation. This is made with wood in the CNC.

<img src= "images/piece5.png">
<img src= "images/piece6.png">
<br><br><br>
This piece would be the only 3D printed piece joining the legs with the upper planks and the joint. It is made out of PLA.

### Production
<img src= "images/piecemaking.jpeg">
<br><br><br>
The 3D piece in the making.
1mm nozzle 
White PLA


<img src= "images/pieceprinted.jpeg">
 <br>

<img src= "images/processtable.jpeg">

## Details
<img src= "images/detailshot.jpeg">
<img src= "images/detaillegsmounted.jpeg">

## Iteration Process
In the beginning we only designed the joint to hold two ends, but them we had to account for the legs supporting the head and the tail of the table so we created a new add-on 3d printed part to add on top of the previous one. 
<img src= "images/detaillegs.jpeg">
## 1:1 Model
<img src= "images/birdseyeview.jpeg">
<img src= "images/fullpiece.jpeg">

## Future Development

To use it in common exhibitions as a piece to support the main work. <br>
The objective in the near future is for the use of MDEF students in their Design Dialogues Exhibition.







